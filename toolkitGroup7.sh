#!/bin/bash

# Este programa se llama Toolkit Sysadm
# Es un kit de herramientas de gestion de usuarios, discos, procesos, memoria y texto para tener en cuenta al momento de necesitar dichas herramientas. 
# Fue desarrollado por los alumnos de la materia Automatizacion y Scripting de la Facultad de Informatica en la Universidad Del Comahue.
# Desarrolladores: Emiliano Camusso, Maximiliano Espinoza, Fernando Boezio.
#
#

ip0=10.0.18.109
ip1=10.0.18.113

# Menu de la gestion de usuarios
function menuUsuarios {
clear
echo -e "1) Altas de usuarios \n";
echo -e "2) Cruce de claves \n";
echo -e "3) Baja de usuarios \n";
echo -e "4) Reporte de usuarios de sistema \n";
echo -e "5) Reporte de usuarios activos \n";
echo -e "0) Volver al menu anterior \n";
echo -ne "Opcion: ";
read -n 1 opcion

case $opcion in
0) break;;
1) addUsers;;
2) cruceLlaves;;
3) bajaUsers;;
4) users;;
esac
}

#Funcion para crear usuarios dado el listado .cvs
function addUsers {
clear
if [ "$EUID" -ne 0 ] 	# Si la variable tiene valor 0 se inicio como root
	then
clear
	echo "#####################################################################"
	echo -e "\nUSTED NO INICIO EL PROGRAMA COMO ROOT, NO TIENE ACCESO A ESTA OPCION\n"
	echo "#####################################################################"
	else 		# Si el programa fue iniciado como root, procede a realizar el adduser
echo -e "Inserte la lista para crear usuarios con extension .csv: ";
read lista				# Va a almacenar la lista dada en la variable $lista
declare -a crearUsuarios=( $(cat $lista |tr "," " ") )		# Se declara la variable como array, se eliminan las comas y se agrega un espacio
local columnas=( $(cat $lista | tr "," " " | wc -w) )		# Se almacena el valor de las columnas dentro de la lista dada para el loop for
echo -e "Desea agregar los usuarios en esta maquina? [S/N]: ";
read sino
	if [ $sino == "S" ] || [ $sino == "s" ]
		then
		for (( i=0; i<$columnas; i++ ))	# Loop for para aumentar el valor de la variable $i para poder crear X cantidad de usuarios
			do
				/sbin/adduser "${crearUsuarios[$i]}"
			done
		else
		echo -e "En que maquina virtual desea agregar los usuarios?  [ ip0 / ip1 ]: ";
		read ip
			if [ $ip == "ip0" ]
			then
			for (( i=0; i<$columnas; i++ ))
			do
			ssh camusso10@$ip0 "/sbin/adduser "${crearUsuarios[$i]}""
			done
			elif [ $ip == "ip1" ]
			then
			for (( i=0; i<$columnas; i++ ))
			do
			ssh camusso10@$ip1 "/sbin/adduser "${crearUsuarios[$i]}""
			done
			fi
	fi
fi
}

function cruceLlaves {
clear
echo -e "Inserte la lista .csv de usuarios que desea intercambiar las llaves de ssh: ";
read lista
declare -a cruceKeys=( $(cat $lista | tr "," " ") )
local columnas=( $(cat $lista | tr "," " " | wc -w) )
	echo -e "\nA que maquina desea cruzar las llaves? [ ip0 / ip1 ]: \n"
	read ip
	if [ $ip == "ip0" ]
		then
		for (( i=0; i<$columnas; i++ ))
		do
			if [ -e /home/${cruceKeys[$i]}/.ssh/${cruceKeys[$i]}*.pub ]
			then
			su ${cruceKeys[$i]} -c "ssh-copy-id ${cruceKeys[$i]}@$ip0"
			else
			su ${cruceKeys[$i]} -c "ssh-keygen ; ssh-copy-id ${cruceKeys[$i]}@$ip0"
			fi
		done
		elif [ $ip == "ip1" ]
		then
		for (( i=0; i<$columnas; i++ ))
		do
			if [ -e /home/${cruceKeys[$i]}/.ssh/${cruceKeys[$i]}*.pub ]
			then
			su ${cruceKeys[$i]} -c "ssh-copy-id ${cruceKeys[$i]}@$ip1"
			else
			su ${cruceKeys[$i]} -c "ssh-keygen ; ssh-copy-id ${cruceKeys[$i]}@$ip1"
			fi
		done
	fi
}

function bajaUsers {
clear
if [ "$EUID" -ne 0 ] 	# Si la variable tiene valor 0 se inicio como root
	then
clear
	echo "#####################################################################"
	echo -e "\nUSTED NO INICIO EL PROGRAMA COMO ROOT, NO TIENE ACCESO A ESTA OPCION\n"
	echo "#####################################################################"
	else 		# Si el programa fue iniciado como root, procede a realizar el adduser
echo -e "Inserte la lista con extension .csv para eliminar usuarios: ";
read lista			# Va a almacenar la lista dada en la variable $lista
declare -a delUsers=( $(cat $lista |tr "," " ") )		# Se declara la variable como array, se eliminan las comas y se agrega un espacio
local columnas=( $(cat $lista | tr "," " " | wc -w) )		# Se almacena el valor de las columnas dentro de la lista dada para el loop for
	for (( i=0; i<$columnas; i++ ))
	do
		/sbin/deluser "${delUsers[$i]}"
	done
fi
}

function users {
clear
echo -e "###############################"
echo -e "# Listado     de     usuarios #"
echo -e "###############################\n"

	compgen -u | sort
}

# Funcion para el menu de gestion de discos 
function gDiscos {
	clear # Limpia la pantalla
echo -e "Falta el menu de gestor de discos \n"; # Imprime un mensaje del menu de gestor de discos 
}

#Funcion para el menu de gestion de procesos
function gProcesos {
	clear # Limpia la pantalla
echo -e "Falta el menu de gestor de procesos \n"; # Imprime un mensaje del menu de gestor de procesos
}

#Funcion para el menu de gestion de memoria
function gMemoria {
	clear # Limpia la pantalla
echo -e "Falta el menu de gestor de memoria \n"; # Imprime un mensaje del menu de gestor de memoria
}

#Funcion para el menu de gestion de texto
function gTexto {
	clear # Limpia la pantalla
echo -e "Falta el menu de gestor de texto \n"; # Imprime un mensaje del menu de gestor de texto
}

# La funcion menu imprime las opciones a las que se pueden acceder de este programa
function menu {

clear
echo -e "################################################"
echo -e "#		Toolkit SYS ADM		       #"
echo -e "################################################\n"
echo -e "1) Gestion de usuarios \n"
echo -e "3) Gestion de discos \n"
echo -e "4) Gestion de procesos \n"
echo -e "5) Gestion de memoria \n"
echo -e "6) Gestion de texto \n"
echo -e "0) Salir del programa \n"
echo -ne "Opcion: "
read -n 1 opcion
}

while [ 1 ] 
	do
		menu	# Imprime el menu en loop #
case $opcion in 		# Va a utilizar como entrada el valor que contiene 'read' del menu para utilizar las funciones #
0) break ;; 	# Rompe el loop del menu #
1) menuUsuarios;;		# Llama a la funcion gUsuarios #
3) gDiscos;;		# Llama a la funcion gDiscos #
4) gProcesos;;		# Llama a la funcion gProcesos #
5) gMemoria;;		# Llama a la funcion gMemoria #
6) gTexto;;		# Llama a la funcion gTexto #
*)
clear		# Limpia la pantalla despues de seleccionar una opcion #
echo -e "Opcion invalida\n"		# Mensaje cuando el input es incorrecto con las opciones del menu #
esac
echo -ne "\nPresione cualquier tecla para continuar\n"		# Por el momento loopea de vuelta hacia el menu #
read -n 1 line
	done
		clear
